export TERM="screen-256color"
export PATH="$HOME/.local/bin:$HOME/.bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
export NVM_LAZY_LOAD=true

autoload -U colors && colors

##
# Antigen
##

if [[ "$OSTYPE" == "linux-gnu" ]]; then
   source $HOME/antigen.zsh
elif [[ "$OSTYPE" == "darwin"* ]]; then
   source $(brew --prefix)/share/antigen/antigen.zsh
fi

# Geneal Plugins
antigen bundle git
antigen bundle jsontools
antigen bundle autojump
antigen bundle compleat
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-completions src
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle lukechilds/zsh-nvm

# Node Plugins
antigen bundle npm

# Docker Plugins
antigen bundle docker
antigen bundle docker-compose

# Theme
antigen bundle mafredri/zsh-async
antigen bundle achannarasappa/pure

antigen apply

##
# Language
##

# Go
export GOROOT="/usr/local/go"
export GOPATH="$HOME/repositories/go"
export PATH="$PATH:$GOROOT/bin"

##
# Completion
##
autoload -U compinit
compinit
zmodload -i zsh/complist
setopt hash_list_all            # hash everything before completion
setopt completealiases          # complete alisases
setopt always_to_end            # when completing from the middle of a word, move the cursor to the end of the word
setopt complete_in_word         # allow completion from within a word/phrase
setopt correct                  # spelling correction for commands
setopt list_ambiguous           # complete as much of a completion until it gets ambiguous.

zstyle ':completion::complete:*' use-cache on               # completion caching, use rehash to clear
zstyle ':completion:*' cache-path ~/.zsh/cache              # cache path
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'   # ignore case
zstyle ':completion:*' menu select=2                        # menu if nb items > 2
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}       # colorz !
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate # list of completers to use

DEFAULT_USER=`whoami`
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

##
# Integration
##

# nvm
autoload -U add-zsh-hook
load-nvmrc() {
  if [[ -f .nvmrc && -r .nvmrc ]]; then
    nvm use
  elif [[ $(nvm version) != $(nvm version default)  ]]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc

##
# Aliases
##

# General
alias clip='xclip -sel clip'
alias paste='xclip -o'
alias dir='echo "${PWD##*/}"'
alias vim='nvim'
alias e='nvim'

# jq
jq_flatten_json_files(){
  find . -name '*.json' -exec cat '{}' + | jq -s '. | flatten' "$@"
}

# Docker
alias d='docker'
alias dc='docker-compose'
alias db='docker build -t "${PWD##*/}" $@ .'
alias dr='docker run --rm -p 8000:80 -P -it --name "${PWD##*/}" $@ "${PWD##*/}"'

# Git
alias gi='git update-index --assume-unchanged $1'

# Redis
alias rget='redis-cli -h localhost -p 6379 get $1'
alias rstart='docker run --rm -p 6379:6379 -v /home/ani/datastore:/data --name redis redis'

# Productivity
alias watch='git ls-files | entr $@'

# Hardware
alias disable_webcam='sudo modprobe -r uvcvideo'
alias enable_webcam='sudo modprobe uvcvideo'

# Network
alias process_port='netstat -tulpn | grep $1'

# Applications
alias chrome=/opt/google/chrome/google-chrome --enable-plugins
alias cross-database-export='docker run --net=host -it --rm -e APP_ENV=staging -e APP_KEY=$(cat /opt/ejson/keys/5dab64968056df6f68ca0eda5737b3d830c901cb0d1051cfd5ef246363ed6834) em-utility-cross-datastore-export'

# Work - Database
alias pg='psql -h localhost -U achannarasappa exm-production $@'

# Work - Network
alias vpn-bv='sudo openvpn --config $HOME/.ovpn/bv.ovpn'

# Work - Operations
alias dp='docker build -t quay.io/excelmicro/$(dir):master . && docker push quay.io/excelmicro/$(dir)'

# Work - Web
alias bb='chrome https://bitbucket.org/fusemail/${PWD##*/} > /dev/null'
alias j='chrome https://fusemail.atlassian.net/issues/?jql=component%20%3D%20${PWD##*/}%20ORDER%20BY%20created%20DESC%2C%20lastViewed%20DESC > /dev/null'
alias pr='chrome https://bitbucket.org/fusemail/${PWD##*/}/pull-requests/ > /dev/null'

##
# Tools
##

# elixir version manager
test -s "$HOME/.kiex/scripts/kiex" && source "$HOME/.kiex/scripts/kiex"

# direnv
eval "$(direnv hook zsh)"